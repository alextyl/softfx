package com.junior.softfx.realm.data;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;


public class News extends RealmObject {


    private int id;

    @PrimaryKey
    private String title;

    private String link;
    private String description;
    private long time;

    public News(){}

    public News(int id, String title, String description, long time, String link) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.time = time;
        this.link = link;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
}

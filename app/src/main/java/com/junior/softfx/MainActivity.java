package com.junior.softfx;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import com.junior.softfx.parser.Parser;
import com.junior.softfx.adapters.AnalyticsAdapter;
import com.junior.softfx.adapters.NewsAdapter;
import com.junior.softfx.callbaks.AnalyticsCallback;
import com.junior.softfx.callbaks.DowloadAnalyticsCallback;
import com.junior.softfx.callbaks.DownloadNewsCallback;
import com.junior.softfx.callbaks.NewsCallback;
import com.junior.softfx.realm.data.Analytics;
import com.junior.softfx.realm.data.News;
import com.pkmmte.pkrss.PkRSS;

import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;


public class MainActivity extends AppCompatActivity implements DowloadAnalyticsCallback, DownloadNewsCallback {

    private final String urlAnalytics = "https://widgets.spotfxbroker.com:8088/GetAnalyticsRss?domain=fxopen.com";
    private final String urlNews = "https://widgets.spotfxbroker.com:8088/GetLiveNewsRss?domain=fxopen.com";

    private RecyclerView mRecyclerView;
    private LinearLayoutManager mLayoutManager;
    private AnalyticsAdapter mAnalyticsAdapter;
    private NewsAdapter mNewsAdapter;
    boolean isLoading = false;
    private int visibleThreshold = 5;
    int totalItemCountAnalytics,lastVisibleItemAnalytics, totalItemCountNews,lastVisibleItemNews;
    private ArrayList<Analytics> mAnalyticsItem = new ArrayList<>(), mTempAnalytics = new ArrayList<>();
    private ArrayList<News> mTempNews = new ArrayList<>();
    private ArrayList<News> mNewsItem = new ArrayList<>();
    private ProgressBar mProgressBar;
    private PkRSS pkRSS;
    private AnalyticsCallback mAnalyticsCallback;
    private NewsCallback mNewsCallback;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            pkRSS = new PkRSS.Builder(MainActivity.this).parser(new Parser()).build();
            if(mAnalyticsCallback != null)
                mAnalyticsCallback.isCancel = true;
            if(mNewsCallback != null)
                mNewsCallback.isCancel = true;
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    mAnalyticsCallback = new AnalyticsCallback(MainActivity.this);
                    pkRSS.load(urlAnalytics).callback(mAnalyticsCallback).async();
                    return true;
                case R.id.navigation_dashboard:
                    mNewsCallback = new NewsCallback(MainActivity.this);
                    pkRSS.load(urlNews).callback(mNewsCallback).async();
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mProgressBar = findViewById(R.id.progressBar);
        mRecyclerView = findViewById(R.id.recycler_view);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(mRecyclerView.getContext(),
                new LinearLayoutManager(this).getOrientation());
        mRecyclerView.addItemDecoration(dividerItemDecoration);

        new PkRSS.Builder(this).parser(new Parser()).build().load(urlAnalytics).callback(new AnalyticsCallback(this)).async();

        BottomNavigationView navigation = findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
    }


    @Override
    public void analyticsDownload(ArrayList<Analytics> analytics) {

        SharedPreferences sp = getSharedPreferences("WIDGET", MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putLong("TIME", System.currentTimeMillis());
        editor.commit();

        mTempAnalytics = analytics;
        mAnalyticsAdapter = new AnalyticsAdapter(mTempAnalytics, this);
        mRecyclerView.setAdapter(mAnalyticsAdapter);
        mProgressBar.setVisibility(View.GONE);
        mRecyclerView.setVisibility(View.VISIBLE);
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                totalItemCountAnalytics = mLayoutManager.getItemCount();
                lastVisibleItemAnalytics = mLayoutManager.findLastVisibleItemPosition();

                if (!isLoading && totalItemCountAnalytics <= (lastVisibleItemAnalytics + visibleThreshold)) {
                    loadAnalytics();
                    isLoading = true;
                }

            }
        });
    }

    private void loadAnalytics() {
        mTempAnalytics.add(null);
        mAnalyticsAdapter.notifyItemInserted(mTempAnalytics.size() - 1);

        Realm realm = Realm.getDefaultInstance();
        RealmResults<Analytics> realmResults = realm.where(Analytics.class).sort("time", Sort.DESCENDING).findAll();
        mAnalyticsItem.addAll(realmResults);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mTempAnalytics.remove(mTempAnalytics.size() - 1);
                mAnalyticsAdapter.notifyItemRemoved(mTempAnalytics.size());
                int Total = mAnalyticsItem.size();
                int start = mTempAnalytics.size();
                int end = start + 10;
                int size = (Total > end) ? end : Total;

                for (int i = start; i < size; i++) {
                    mTempAnalytics.add(mAnalyticsItem.get(i));
                }
                isLoading = (Total == size);
                mAnalyticsAdapter.refreshAdapter(isLoading,mTempAnalytics);
            }
        }, 2000);
    }

    @Override
    public void analyticsDownloadError() {
        Realm realm = Realm.getDefaultInstance();
        RealmResults<Analytics> realmResults = realm.where(Analytics.class).sort("time", Sort.DESCENDING).findAll();
        ArrayList<Analytics> analytics = new ArrayList<>();
        analytics.addAll(realmResults);
        analyticsDownload(analytics);
    }

    @Override
    public void inProgress() {
        mProgressBar.setVisibility(View.VISIBLE);
        mRecyclerView.setVisibility(View.GONE);
    }


    @Override
    public void newsDownload(ArrayList<News> news) {

        SharedPreferences sp = getSharedPreferences("WIDGET", MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putLong("TIME", System.currentTimeMillis());
        editor.commit();

        mTempNews = news;
        mNewsAdapter = new NewsAdapter(mTempNews, this);
        mRecyclerView.setAdapter(mNewsAdapter);
        mProgressBar.setVisibility(View.GONE);
        mRecyclerView.setVisibility(View.VISIBLE);
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                totalItemCountNews = mLayoutManager.getItemCount();
                lastVisibleItemNews = mLayoutManager.findLastVisibleItemPosition();

                if (!isLoading && totalItemCountNews <= (lastVisibleItemNews + visibleThreshold)) {
                    loadNews();
                    isLoading = true;
                }

            }
        });
    }


    public void loadNews() {
        mTempNews.add(null);
        mNewsAdapter.notifyItemInserted(mTempNews.size() - 1);

        Realm realm = Realm.getDefaultInstance();
        RealmResults<News> realmResults = realm.where(News.class).sort("time", Sort.DESCENDING).findAll();
        mNewsItem.addAll(realmResults);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mTempNews.remove(mTempNews.size() - 1);
                mNewsAdapter.notifyItemRemoved(mTempNews.size());
                int Total = mNewsItem.size();
                int start = mTempNews.size();
                int end = start + 10;
                int size = (Total > end) ? end : Total;

                for (int i = start; i < size; i++) {
                    mTempNews.add(mNewsItem.get(i));
                }
                isLoading = (Total == size);
                mNewsAdapter.refreshAdapter(isLoading,mTempNews);
            }
        }, 2000);
    }


    @Override
    public void newsDownloadError() {
        Realm realm = Realm.getDefaultInstance();
        final RealmResults<News> realmResults = realm.where(News.class).sort("time", Sort.DESCENDING).findAll();

        ArrayList<News> news = new ArrayList<>();
        news.addAll(realmResults);
        newsDownload(news);
    }

}

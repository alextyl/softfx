package com.junior.softfx.adapters;


import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.junior.softfx.Main2Activity;
import com.junior.softfx.R;
import com.junior.softfx.realm.App;
import com.junior.softfx.realm.data.Analytics;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Date;

public class AnalyticsAdapter extends RecyclerView.Adapter  {

    private final int ITEM_VIEW_TYPE_BASIC = 0;
    private final int ITEM_VIEW_TYPE_FOOTER = 1;
    boolean value;

    private ArrayList<Analytics> mAnalyticsList;
    private Activity mActivity;

    public AnalyticsAdapter(ArrayList<Analytics> analyticsList, Activity activity) {
        mAnalyticsList = analyticsList;
        mActivity = activity;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        if (viewType == ITEM_VIEW_TYPE_BASIC) {
            View v = LayoutInflater.from(parent.getContext()).inflate(
                    R.layout.item_analytics, parent, false);

            vh = new CardViewHolder(v);
        } else{
            View v = LayoutInflater.from(parent.getContext()).inflate(
                    R.layout.progress_bar, parent, false);

            vh = new ProgressViewHolder(v);
        }

        return vh;
    }

    public void refreshAdapter(boolean value, ArrayList<Analytics> tempCardItem){
        this.value = value;
        this.mAnalyticsList = tempCardItem;
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof CardViewHolder) {

            final Analytics analyticItem = mAnalyticsList.get(position);

            ((CardViewHolder) holder).mTitle.setText(analyticItem.getTitle());
            ((CardViewHolder) holder).mDate.setText(App.dateFormat.format(new Date(analyticItem.getTime())));
            ((CardViewHolder) holder).mDescription.setText(analyticItem.getDescription());
            ((CardViewHolder) holder).mLink.setText(analyticItem.getLink());
            Picasso.get().load(analyticItem.getImage()).placeholder(R.drawable.placeholder).into(((CardViewHolder) holder).mPhoto);

            ((CardViewHolder) holder).mLink.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(mActivity, Main2Activity.class);
                    intent.putExtra("link", analyticItem.getLink());

                    mActivity.startActivity(intent);
                }
            });

        } else {
            if (!value) {
                ((ProgressViewHolder) holder).progressBar.setVisibility(View.VISIBLE);
                ((ProgressViewHolder) holder).progressBar.setIndeterminate(true);
            } else ((ProgressViewHolder) holder).progressBar.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemViewType(int position) {
        return mAnalyticsList.get(position) != null ? ITEM_VIEW_TYPE_BASIC : ITEM_VIEW_TYPE_FOOTER;
    }

    @Override
    public int getItemCount() {
        return mAnalyticsList == null ? 0: mAnalyticsList.size();
    }


    public static class CardViewHolder extends RecyclerView.ViewHolder {
        TextView mTitle;
        TextView mDate;
        TextView mDescription;
        TextView mLink;
        ImageView mPhoto;

        public CardViewHolder(View v) {
            super(v);
            mTitle = v.findViewById(R.id.textView);
            mDate = v.findViewById(R.id.textView2);
            mDescription = v.findViewById(R.id.textView3);
            mLink = v.findViewById(R.id.textView4);
            mPhoto = v.findViewById(R.id.imageView);
        }
    }

    public static class ProgressViewHolder extends RecyclerView.ViewHolder {
        ProgressBar progressBar;
        public ProgressViewHolder(View v) {
            super(v);
            progressBar = v.findViewById(R.id.progress_bar);
        }
    }
}

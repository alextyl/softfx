package com.junior.softfx.adapters;


import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.junior.softfx.Main2Activity;
import com.junior.softfx.R;
import com.junior.softfx.realm.App;
import com.junior.softfx.realm.data.News;

import java.util.ArrayList;
import java.util.Date;

public class NewsAdapter extends RecyclerView.Adapter{

    private final int ITEM_VIEW_TYPE_BASIC = 0;
    private final int ITEM_VIEW_TYPE_FOOTER = 1;
    boolean value;

    private ArrayList<News> mNewsArrayList;
    private Activity mActivity;

    public NewsAdapter(ArrayList<News> newsArrayList, Activity activity) {
        mNewsArrayList = newsArrayList;
        mActivity = activity;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        if (viewType == ITEM_VIEW_TYPE_BASIC) {
            View v = LayoutInflater.from(parent.getContext()).inflate(
                    R.layout.item_news, parent, false);

            vh = new NewsAdapter.CardViewHolder(v);
        } else{
            View v = LayoutInflater.from(parent.getContext()).inflate(
                    R.layout.progress_bar, parent, false);

            vh = new NewsAdapter.ProgressViewHolder(v);
        }

        return vh;
    }

    public void refreshAdapter(boolean value, ArrayList<News> tempCardItem){
        this.value = value;
        this.mNewsArrayList = tempCardItem;
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof NewsAdapter.CardViewHolder) {

            final News news = mNewsArrayList.get(position);

            ((NewsAdapter.CardViewHolder) holder).mTitle.setText(news.getTitle());
            ((NewsAdapter.CardViewHolder) holder).mDate.setText(App.dateFormat.format(new Date(news.getTime())));
            ((NewsAdapter.CardViewHolder) holder).mDescription.setText(news.getDescription());
            ((CardViewHolder) holder).mLink.setText(news.getLink());

            ((NewsAdapter.CardViewHolder) holder).mLink.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(mActivity, Main2Activity.class);
                    intent.putExtra("link", news.getLink());

                    mActivity.startActivity(intent);
                }
            });

        } else {
            if (!value) {
                ((NewsAdapter.ProgressViewHolder) holder).progressBar.setVisibility(View.VISIBLE);
                ((NewsAdapter.ProgressViewHolder) holder).progressBar.setIndeterminate(true);
            } else ((NewsAdapter.ProgressViewHolder) holder).progressBar.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemViewType(int position) {
        return mNewsArrayList.get(position) != null ? ITEM_VIEW_TYPE_BASIC : ITEM_VIEW_TYPE_FOOTER;
    }

    @Override
    public int getItemCount() {
        return mNewsArrayList == null ? 0: mNewsArrayList.size();
    }


    public static class CardViewHolder extends RecyclerView.ViewHolder {
        TextView mTitle;
        TextView mDate;
        TextView mDescription;
        TextView mLink;

        public CardViewHolder(View v) {
            super(v);
            mTitle = v.findViewById(R.id.textView);
            mDate = v.findViewById(R.id.textView2);
            mDescription = v.findViewById(R.id.textView3);
            mLink = v.findViewById(R.id.textView4);
        }
    }

    public static class ProgressViewHolder extends RecyclerView.ViewHolder {
        ProgressBar progressBar;
        public ProgressViewHolder(View v) {
            super(v);
            progressBar = v.findViewById(R.id.progress_bar);
        }
    }

}

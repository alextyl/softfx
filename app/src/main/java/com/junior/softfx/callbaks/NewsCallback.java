package com.junior.softfx.callbaks;


import android.support.annotation.NonNull;

import com.junior.softfx.realm.data.News;
import com.pkmmte.pkrss.Article;
import com.pkmmte.pkrss.Callback;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;

public class NewsCallback implements Callback {

    private DownloadNewsCallback mDowloadCallback;
    public boolean isCancel;

    public NewsCallback(DownloadNewsCallback dowloadCallback) {
        mDowloadCallback = dowloadCallback;
    }

    @Override
    public void onPreload() {
        mDowloadCallback.inProgress();
    }

    @Override
    public void onLoaded(List<Article> newArticles) {

        if(isCancel) return;

        final Realm realm = Realm.getDefaultInstance();

        final RealmResults<News> realmResults = realm.where(News.class).sort("time", Sort.DESCENDING).findAll();

        int size = realmResults.size();

        if(size > 100){
            for(int i = 100; i < size; i++){
                final int finalI = i;
                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        realmResults.get(finalI).deleteFromRealm();
                    }
                });
            }
        }

        final ArrayList<News> newsArrayList = new ArrayList<>();
        for(Article article: newArticles){
            final News news = new News(article.getId(),
                    article.getTitle(),
                    article.getDescription(),
                    article.getDate(),
                    article.getSource().toString());
            newsArrayList.add(news);
        }

        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(@NonNull Realm realm) {
                for(News news: newsArrayList){
                    realm.copyToRealmOrUpdate(news);
                }
            }
        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                mDowloadCallback.newsDownload(newsArrayList);
            }
        });
    }

    @Override
    public void onLoadFailed() {
        mDowloadCallback.newsDownloadError();
    }
}

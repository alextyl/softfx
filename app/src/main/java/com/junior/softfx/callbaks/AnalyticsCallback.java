package com.junior.softfx.callbaks;

import android.support.annotation.NonNull;
import android.util.Log;

import com.junior.softfx.realm.data.Analytics;
import com.junior.softfx.realm.data.News;
import com.pkmmte.pkrss.Article;
import com.pkmmte.pkrss.Callback;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;

public class AnalyticsCallback implements Callback {

    private DowloadAnalyticsCallback mDowloadCallback;
    public boolean isCancel;

    public AnalyticsCallback(DowloadAnalyticsCallback dowloadCallback) {
        mDowloadCallback = dowloadCallback;
    }

    @Override
    public void onPreload() {
        mDowloadCallback.inProgress();
    }

    @Override
    public void onLoaded(final List<Article> newArticles) {

        if(isCancel) return;

        final Realm realm = Realm.getDefaultInstance();

        final RealmResults<News> realmResults = realm.where(News.class).sort("time", Sort.DESCENDING).findAll();

        int size = realmResults.size();
        int realmOver = size - 100;

        if(size > 100){
            for(int i = 0; i < realmOver; i++){
                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        realmResults.get(100).deleteFromRealm();
                    }
                });
            }
        }

        final ArrayList<Analytics> analytics = new ArrayList<>();
        for(Article article: newArticles){
            final Analytics analytic = new Analytics(article.getId(),
                    article.getImage().toString(),
                    article.getTitle(),
                    article.getDescription(),
                    article.getDate(),
                    article.getSource().toString());
            analytics.add(analytic);
        }

        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(@NonNull Realm realm) {
                for(Analytics article: analytics){
                    realm.copyToRealmOrUpdate(article);
                }
            }
        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                mDowloadCallback.analyticsDownload(analytics);
            }
        });
    }

    @Override
    public void onLoadFailed() {
        mDowloadCallback.analyticsDownloadError();
    }
}
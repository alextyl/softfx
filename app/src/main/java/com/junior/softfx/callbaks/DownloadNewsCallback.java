package com.junior.softfx.callbaks;


import com.junior.softfx.realm.data.News;

import java.util.ArrayList;

public interface DownloadNewsCallback {

    void newsDownload(ArrayList<News> news);
    void newsDownloadError();
    void inProgress();

}

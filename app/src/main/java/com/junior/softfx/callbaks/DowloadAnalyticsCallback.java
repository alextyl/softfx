package com.junior.softfx.callbaks;


import com.junior.softfx.realm.data.Analytics;

import java.util.ArrayList;


public interface DowloadAnalyticsCallback {

    void analyticsDownload(ArrayList<Analytics> analytics);
    void analyticsDownloadError();
    void inProgress();

}

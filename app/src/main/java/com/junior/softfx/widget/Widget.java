package com.junior.softfx.widget;


import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.RemoteViews;

import com.junior.softfx.MainActivity;
import com.junior.softfx.R;

import java.util.Arrays;
import java.util.Date;

public class Widget extends AppWidgetProvider {


    final String LOG_TAG = "myLogs";

    @Override
    public void onEnabled(Context context) {
        super.onEnabled(context);

        Log.d(LOG_TAG, "onEnabled");
    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager,
                         int[] appWidgetIds) {
        super.onUpdate(context, appWidgetManager, appWidgetIds);

        SharedPreferences sp = context.getSharedPreferences("WIDGET", Context.MODE_PRIVATE);
        long widgetText = sp.getLong("TIME", 0);

        final int N = appWidgetIds.length;

        for (int appWidgetId : appWidgetIds) {
            Intent intent = new Intent(context, MainActivity.class);
            PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);

            RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.widget);
            views.setOnClickPendingIntent(R.id.tv, pendingIntent);

            appWidgetManager.updateAppWidget(appWidgetId, views);

            RemoteViews widgetView = new RemoteViews(context.getPackageName(),
                    R.layout.widget);
            widgetView.setTextViewText(R.id.tv, new Date(widgetText).toString());

            appWidgetManager.updateAppWidget(appWidgetIds, widgetView);
        }


        Log.d(LOG_TAG, "onUpdate " + Arrays.toString(appWidgetIds));
    }

    @Override
    public void onDeleted(Context context, int[] appWidgetIds) {
        super.onDeleted(context, appWidgetIds);
        Log.d(LOG_TAG, "onDeleted " + Arrays.toString(appWidgetIds));
    }

    @Override
    public void onDisabled(Context context) {
        super.onDisabled(context);
        Log.d(LOG_TAG, "onDisabled");
    }

}